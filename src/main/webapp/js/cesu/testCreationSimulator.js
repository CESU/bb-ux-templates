jQuery(document).ready(function(){

jQuery('.submit').click(function(e){
	
	// Test values
	let testId;

	// User input
	let testName = 'Test Name';
	let testDesc = 'Test Description';
	let testIns = 'Test Instructions';

	console.log("Stage: 0");
	// Initiate headless native workflow 
	let frameTemplate = jQuery('<iframe id="frame" src="http://localhost:9876/webapps/assessment/do/content/assessment?action=ADD&course_id=_3_1&content_id=_10_1&assessmentType=Test"></iframe>');
	jQuery('body').before(frameTemplate);

	frameTemplate.on('load', function() {

		let frameContents = frameTemplate.contents();

		// When on the Create Test screen 
		if (jQuery('form[name="assessmentForm"][action*="addAssessment"]', frameContents)[0]) {
			console.log("Stage: 1");
			if (!testId) {
				console.log("Stage: 2");
				// Find current test if created
				// Used to determine test creation stage
				jQuery('#assessmentId option', frameContents).each(function(key, value) {
					if (value.innerHTML === testName) {
						testId = value.value;

						// Break loop
						return false;
					}
				});
			}

			if (!testId) {
				console.log("Stage: 3.1");
			    // Navigate to create new test
			    let btn = jQuery('form[name="assessmentForm"] li > div.field > a', frameContents)[0];
			    console.log("Stage: 3.2");
			    btn.click();
			    console.log("Stage: 3.3");
			}

			// Add test to course
			if (testId) {
				console.log("Stage: 6");
				// Select option
				jQuery('#assessmentId', frameContents).val(testId);

				// Submit
				jQuery('#bottom_Submit', frameContents)[0].click();
			}
		}

		// When on Test Information screen
		if (jQuery('form[name="assessmentInfoForm"]', frameContents)[0]) {
			console.log("Stage: 4");
		    // Fill meta data
		    jQuery('#assessment_name_input', frameContents).val(testName);
		    jQuery('p', jQuery('#descriptiontext_ifr', frameContents).contents()).html(testDesc);
		    jQuery('p', jQuery('#instructionstext_ifr', frameContents).contents()).html(testIns);

		    jQuery('#bottom_Submit', frameContents)[0].click();
		}

		// When on Test Canvas
		if (jQuery('#bb-assessment-canvas', frameContents)[0]) {
			console.log("Stage: 5");

			// Return to Create Test screen
			jQuery('.backLink > a', frameContents)[0].click();
		}

	    // TODO: Add questions
	    // TODO: Use switch to load template based on enum 
	    // TODO: Default to empty quiz

	    // When on Test Options screen
	    if (jQuery('form[name="assessmentForm"][action*="saveOptions"]', frameContents)[0]) {
			console.log("Stage: 7");
	    	// Finalize adding Test to course
	    	jQuery('#bottom_Submit', frameContents)[0].click();

	    	frameTemplate.remove();
	    }
	});
});
});