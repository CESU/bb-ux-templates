<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

<%@ taglib uri="/bbNG" prefix="bbNG" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<fmt:setLocale value="${language}" />
<fmt:setBundle basename="text" />


<fmt:message key="template.inputform.page.title" var="pageTitle"/>
<fmt:message key="template.inputform.pageHeader.title" var="headerTitle"/>
<fmt:message key="template.inputform.pageHeader.instructions" var="headerInstructions"/>
<fmt:message key="template.inputform.pageHeader.breadcrumb.title" var="breadcrumbTitle"/>

<fmt:message key="general.meta.credits" var="credits"/>

<fmt:message key="template.inputform.testinformation.tabtitle" var="infoTabTitle"/>
<fmt:message key="template.inputform.testinformation.steptitle" var="infoStepTitle"/>
<fmt:message key="template.inputform.testinformation.stepinstructions" var="infoStepInstructions"/>
<fmt:message key="template.inputform.testinformation.input.name" var="infoInputName"/>
<fmt:message key="template.inputform.testinformation.input.description" var="infoInputDescription"/>

<fmt:message key="template.inputform.submit" var="formSubmit"/>


<!DOCTYPE html>
<bbNG:learningSystemPage title="${ pageTitle }" ctxId="ctx">

	<bbNG:cssFile href="styling.css"/>
	
	<bbNG:pageHeader instructions="${ headerInstructions }">
		<bbNG:breadcrumbBar environment="COURSE" navItem="course_plugin_manage">
			<bbNG:breadcrumb title="${ breadcrumbTitle }" />
		</bbNG:breadcrumbBar>
		<bbNG:pageTitleBar showTitleBar="true" title="${ headerTitle }" />
		<div align='right'>
			<a href='http://cesu.au.dk/' target='_blank'>
				<span style="color:#ffffff;">${ credits }</span>
			</a>
		</div>
	</bbNG:pageHeader>
		
	<bbNG:form 
		action=""
		method="POST" 
		onsubmit=""
		id="cesuTestTemplateForm">
		
		<bbNG:dataCollection showSubmitButtons="true">
		
			<%-- Basic Information and options for the test --%>
			<bbNG:stepGroup title="${ infoTabTitle }">
				
				<%-- Basic Information --%>
				<bbNG:step title="${ infoStepTitle }" instructions="${ infoStepInstructions }" >
				
				    	    
				    <bbNG:dataElement label="${ infoInputName }" isRequired="true">
				      	<bbNG:textElement name="heading" size="75"/>
				    </bbNG:dataElement>
				    
					
				    <bbNG:dataElement label="${ infoInputDescription }">
				    	<textarea rows="6" cols="75" name="body"></textarea>
				    </bbNG:dataElement>
				    
				</bbNG:step>
				
				<%-- Basic Options --%>
				<bbNG:step title="BASIC OPTIONS STEP TITLE">
				
					<%-- Answer numbering --%>
					<bbNG:dataElement>
						<bbNG:selectElement name="ANSWER NUMBERING DROPDOWN">
							<bbNG:selectOptionElement value="VAL1"/>
							<bbNG:selectOptionElement value="VAL2"/>
							<bbNG:selectOptionElement value="VAL3"/>
							<bbNG:selectOptionElement value="VAL4"/>
							<bbNG:selectOptionElement value="VAL5"/>
						</bbNG:selectElement>
					</bbNG:dataElement>
					
					<%-- Answer orientation --%>
					<bbNG:dataElement>
						<bbNG:selectElement name="ANSWER ORIENTATION DROPDOWN">
							<bbNG:selectOptionElement value="VAL1"/>
							<bbNG:selectOptionElement value="VAL2"/>
						</bbNG:selectElement>
					</bbNG:dataElement>
				</bbNG:step>
				
			</bbNG:stepGroup>
	
			<%-- 1st mcq question --%>
  	   		<bbNG:stepGroup title="QUESTION1">
  	   		
  	   			<%-- The question --%>
				<bbNG:step title=" mcqQuestionStepTitle" instructions="${ mcqQuestionStepInstructions }" >
					<bbNG:dataElement label="${ questionInputTitle }">
				      	<bbNG:textElement name="heading" size="75" value=""/>
				    </bbNG:dataElement>
				    
				    <bbNG:dataElement label="${ questionInputText }" isRequired="true">
				    	<textarea rows="6" cols="75" name="body"></textarea>
				    </bbNG:dataElement>
				</bbNG:step>
				
				<%-- Answer step --%>
				<bbNG:step title="ANSWER STEP TITLE">
					<%-- Add answer --%>
					<bbNG:dataElement>
						<bbNG:button label="BTN LABEL"/>
					</bbNG:dataElement>
					
					<%-- Answers --%>
					<table>
						<%-- Table heading --%>
						<thead>
							<tr valign="top">
								<th>
									CORRECT
								</th>
							</tr>
						</thead>
						<%-- 1st default answer --%>
						<tbody>
							<tr valign="top">
								<td align="left">
									<bbNG:dataElement>
										<bbNG:radioElement name="Q1CORRECTANSWER" isSelected="true" value="1"/>
									</bbNG:dataElement>
								</td>
								<td>
									<%-- Answer text input --%>
									<bbNG:dataElement label="ANS1">
										<bbNG:textbox name="Q1A1" />
									</bbNG:dataElement>
								</td>
								<td class="responseFeedback">
									<%-- Feedback text input for the answer --%>
									<bbNG:dataElement label="RESPONSE FEEDBACK">
										<bbNG:textbox name="Q1FB1" />
									</bbNG:dataElement>
								</td>
								<td align="right">
									<%-- Delete answer --%>
									<bbNG:button label="REMOVE ANSWER"/>
								</td>
							</tr>
							<%-- 2nd default answer --%>
							<tr valign="top">
								<td align="left">
									<bbNG:dataElement>
										<bbNG:radioElement name="Q1CORRECTANSWER" value="2"/>
									</bbNG:dataElement>
								</td>
								<td>
									<%-- Answer text input --%>
									<bbNG:dataElement label="ANS2">
										<bbNG:textbox name="Q1A2"/>
									</bbNG:dataElement>
								</td>
								<td class="responseFeedback">
									<%-- Feedback text input for the answer --%>
									<bbNG:dataElement label="RESPONSE FEEDBACK">
										<bbNG:textbox name="Q1FB2" />
									</bbNG:dataElement>
								</td>
								<td align="right">
									<%-- Delete answer --%>
									<bbNG:button label="REMOVE ANSWER"/>
								</td>
							</tr>
							<%-- 3rd default answer --%>
							<tr valign="top">
								<td align="left">
									<bbNG:dataElement>
										<bbNG:radioElement name="Q1CORRECTANSWER" value="3"/>
									</bbNG:dataElement>
								</td>
								<td>
									<%-- Answer text input --%>
									<bbNG:dataElement label="ANS3">
										<bbNG:textbox name="Q1A3"/>
									</bbNG:dataElement>
								</td>
								<td class="responseFeedback">
									<%-- Feedback text input for the answer --%>
									<bbNG:dataElement label="RESPONSE FEEDBACK">
										<bbNG:textbox name="Q1FB3" />
									</bbNG:dataElement>
								</td>
								<td align="right">
									<%-- Delete answer --%>
									<bbNG:button label="REMOVE ANSWER"/>
								</td>
							</tr>
							<%-- 4th default answer --%>
							<tr valign="top">
								<td align="left">
									<bbNG:dataElement>
										<bbNG:radioElement name="Q1CORRECTANSWER" value="4"/>
									</bbNG:dataElement>
								</td>
								<td>
									<%-- Answer text input --%>
									<bbNG:dataElement label="ANS4">
										<bbNG:textbox name="Q1A4"/>
									</bbNG:dataElement>
								</td>
								<td class="responseFeedback">
									<%-- Feedback text input for the answer --%>
									<bbNG:dataElement label="RESPONSE FEEDBACK">
										<bbNG:textbox name="Q1FB4" />
									</bbNG:dataElement>
								</td>
								<td align="right">
									<%-- Delete answer --%>
									<bbNG:button label="REMOVE ANSWER"/>
								</td>
							</tr>
						</tbody>
					</table>
				</bbNG:step>
				
		  	</bbNG:stepGroup>
		  	
		  	<%-- 2nd mcq question --%>
			<bbNG:stepGroup title="QUESTION2">
  	   		
  	   			<%-- The question --%>
				<bbNG:step title=" mcqQuestionStepTitle" instructions="${ mcqQuestionStepInstructions }" >
					<bbNG:dataElement label="${ questionInputTitle }">
				      	<bbNG:textElement name="heading" size="75" value=""/>
				    </bbNG:dataElement>
				    
				    <bbNG:dataElement label="${ questionInputText }" isRequired="true">
				    	<textarea rows="6" cols="75" name="body"></textarea>
				    </bbNG:dataElement>
				</bbNG:step>
				
				<%-- Answer step --%>
				<bbNG:step title="ANSWER STEP TITLE">
					<%-- Add answer --%>
					<bbNG:dataElement>
						<bbNG:button label="BTN LABEL"/>
					</bbNG:dataElement>
					
					<%-- Answers --%>
					<table>
						<%-- Table heading --%>
						<thead>
							<tr valign="top">
								<th>
									CORRECT
								</th>
							</tr>
						</thead>
						<%-- 1st default answer --%>
						<tbody>
							<tr valign="top">
								<td align="left">
									<bbNG:dataElement>
										<bbNG:radioElement name="Q2CORRECTANSWER" isSelected="true" value="1"/>
									</bbNG:dataElement>
								</td>
								<td>
									<%-- Answer text input --%>
									<bbNG:dataElement label="ANS1">
										<bbNG:textbox name="Q2A1" />
									</bbNG:dataElement>
								</td>
								<td class="responseFeedback">
									<%-- Feedback text input for the answer --%>
									<bbNG:dataElement label="RESPONSE FEEDBACK">
										<bbNG:textbox name="Q2FB1" />
									</bbNG:dataElement>
								</td>
								<td align="right">
									<%-- Delete answer --%>
									<bbNG:button label="REMOVE ANSWER"/>
								</td>
							</tr>
							<%-- 2nd default answer --%>
							<tr valign="top">
								<td align="left">
									<bbNG:dataElement>
										<bbNG:radioElement name="Q2CORRECTANSWER" value="2"/>
									</bbNG:dataElement>
								</td>
								<td>
									<%-- Answer text input --%>
									<bbNG:dataElement label="ANS2">
										<bbNG:textbox name="Q2A2"/>
									</bbNG:dataElement>
								</td>
								<td class="responseFeedback">
									<%-- Feedback text input for the answer --%>
									<bbNG:dataElement label="RESPONSE FEEDBACK">
										<bbNG:textbox name="Q2FB2" />
									</bbNG:dataElement>
								</td>
								<td align="right">
									<%-- Delete answer --%>
									<bbNG:button label="REMOVE ANSWER"/>
								</td>
							</tr>
							<%-- 3rd default answer --%>
							<tr valign="top">
								<td align="left">
									<bbNG:dataElement>
										<bbNG:radioElement name="Q2CORRECTANSWER" value="3"/>
									</bbNG:dataElement>
								</td>
								<td>
									<%-- Answer text input --%>
									<bbNG:dataElement label="ANS3">
										<bbNG:textbox name="Q2A3"/>
									</bbNG:dataElement>
								</td>
								<td class="responseFeedback">
									<%-- Feedback text input for the answer --%>
									<bbNG:dataElement label="RESPONSE FEEDBACK">
										<bbNG:textbox name="Q2FB3" />
									</bbNG:dataElement>
								</td>
								<td align="right">
									<%-- Delete answer --%>
									<bbNG:button label="REMOVE ANSWER"/>
								</td>
							</tr>
							<%-- 4th default answer --%>
							<tr valign="top">
								<td align="left">
									<bbNG:dataElement>
										<bbNG:radioElement name="Q2CORRECTANSWER" value="4"/>
									</bbNG:dataElement>
								</td>
								<td>
									<%-- Answer text input --%>
									<bbNG:dataElement label="ANS4">
										<bbNG:textbox name="Q2A4"/>
									</bbNG:dataElement>
								</td>
								<td class="responseFeedback">
									<%-- Feedback text input for the answer --%>
									<bbNG:dataElement label="RESPONSE FEEDBACK">
										<bbNG:textbox name="Q2FB4" />
									</bbNG:dataElement>
								</td>
								<td align="right">
									<%-- Delete answer --%>
									<bbNG:button label="REMOVE ANSWER"/>
								</td>
							</tr>
						</tbody>
					</table>
				</bbNG:step>
				
		  	</bbNG:stepGroup>		  	
		  	
		  	<%-- 3rd mcq question --%>
			<bbNG:stepGroup title="QUESTION3">
  	   		
  	   			<%-- The question --%>
				<bbNG:step title=" mcqQuestionStepTitle" instructions="${ mcqQuestionStepInstructions }" >
					<bbNG:dataElement label="${ questionInputTitle }">
				      	<bbNG:textElement name="heading" size="75" value=""/>
				    </bbNG:dataElement>
				    
				    <bbNG:dataElement label="${ questionInputText }" isRequired="true">
				    	<textarea rows="6" cols="75" name="body"></textarea>
				    </bbNG:dataElement>
				</bbNG:step>
				
				<%-- Answer step --%>
				<bbNG:step title="ANSWER STEP TITLE">
					<%-- Add answer --%>
					<bbNG:dataElement>
						<bbNG:button label="BTN LABEL"/>
					</bbNG:dataElement>
					
					<%-- Answers --%>
					<table>
						<%-- Table heading --%>
						<thead>
							<tr valign="top">
								<th>
									CORRECT
								</th>
							</tr>
						</thead>
						<%-- 1st default answer --%>
						<tbody>
							<tr valign="top">
								<td align="left">
									<bbNG:dataElement>
										<bbNG:radioElement name="Q3CORRECTANSWER" isSelected="true" value="1"/>
									</bbNG:dataElement>
								</td>
								<td>
									<%-- Answer text input --%>
									<bbNG:dataElement label="ANS1">
										<bbNG:textbox name="Q3A1" />
									</bbNG:dataElement>
								</td>
								<td class="responseFeedback">
									<%-- Feedback text input for the answer --%>
									<bbNG:dataElement label="RESPONSE FEEDBACK">
										<bbNG:textbox name="Q3FB1" />
									</bbNG:dataElement>
								</td>
								<td align="right">
									<%-- Delete answer --%>
									<bbNG:button label="REMOVE ANSWER"/>
								</td>
							</tr>
							<%-- 2nd default answer --%>
							<tr valign="top">
								<td align="left">
									<bbNG:dataElement>
										<bbNG:radioElement name="Q3CORRECTANSWER" value="2"/>
									</bbNG:dataElement>
								</td>
								<td>
									<%-- Answer text input --%>
									<bbNG:dataElement label="ANS2">
										<bbNG:textbox name="Q3A2"/>
									</bbNG:dataElement>
								</td>
								<td class="responseFeedback">
									<%-- Feedback text input for the answer --%>
									<bbNG:dataElement label="RESPONSE FEEDBACK">
										<bbNG:textbox name="Q3FB2" />
									</bbNG:dataElement>
								</td>
								<td align="right">
									<%-- Delete answer --%>
									<bbNG:button label="REMOVE ANSWER"/>
								</td>
							</tr>
							<%-- 3rd default answer --%>
							<tr valign="top">
								<td align="left">
									<bbNG:dataElement>
										<bbNG:radioElement name="Q3CORRECTANSWER" value="3"/>
									</bbNG:dataElement>
								</td>
								<td>
									<%-- Answer text input --%>
									<bbNG:dataElement label="ANS3">
										<bbNG:textbox name="Q3A3"/>
									</bbNG:dataElement>
								</td>
								<td class="responseFeedback">
									<%-- Feedback text input for the answer --%>
									<bbNG:dataElement label="RESPONSE FEEDBACK">
										<bbNG:textbox name="Q3FB3" />
									</bbNG:dataElement>
								</td>
								<td align="right">
									<%-- Delete answer --%>
									<bbNG:button label="REMOVE ANSWER"/>
								</td>
							</tr>
							<%-- 4th default answer --%>
							<tr valign="top">
								<td align="left">
									<bbNG:dataElement>
										<bbNG:radioElement name="Q3CORRECTANSWER" value="4"/>
									</bbNG:dataElement>
								</td>
								<td>
									<%-- Answer text input --%>
									<bbNG:dataElement label="ANS4">
										<bbNG:textbox name="Q3A4"/>
									</bbNG:dataElement>
								</td>
								<td class="responseFeedback">
									<%-- Feedback text input for the answer --%>
									<bbNG:dataElement label="RESPONSE FEEDBACK">
										<bbNG:textbox name="Q3FB4" />
									</bbNG:dataElement>
								</td>
								<td align="right">
									<%-- Delete answer --%>
									<bbNG:button label="REMOVE ANSWER"/>
								</td>
							</tr>
						</tbody>
					</table>
				</bbNG:step>
				
		  	</bbNG:stepGroup>		  	
		  	
		  	<%-- Feedback --%>
		  	
		  	<%-- Advanced test options --%>
		  	
			<bbNG:stepSubmit cancelUrl="landing?course_id=${ courseIdExt }" showCancelButton="true">
				<bbNG:stepSubmitButton label="${ formSubmit }" />
			</bbNG:stepSubmit>
			
		</bbNG:dataCollection>
			
	</bbNG:form>
	
	<bbNG:jsFile href="${pageContext.request.contextPath}/js/cesu/testCreationSimulator.js"/>
	
</bbNG:learningSystemPage>