package controllers;


import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import blackboard.data.ReceiptOptions;
import blackboard.platform.plugin.PlugInUtil;
import blackboard.platform.servlet.InlineReceiptUtil;

/**
 * Servlet implementation class LandingPageController
 */
@WebServlet("/mcqTemplatePage")
public class McqTemplateController extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public McqTemplateController() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// verify associated with course
		try {
			if ( !PlugInUtil.authorizeForCourse(request, response) ) {
				return;
			}
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
				
		// init inline message
		ReceiptOptions msg = new ReceiptOptions();
			
		// set the view
		String viewUrl = InlineReceiptUtil.addReceiptToUrl("/WEB-INF/jsp/mcqTemplatePage.jsp", msg);
		
		// forward the user to the view
		RequestDispatcher requestDispatcher = getServletContext().getRequestDispatcher(viewUrl);
		requestDispatcher.forward(request, response);
	}
	
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

	}
}
