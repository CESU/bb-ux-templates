Templates

- Hvordan laves implementeringen?
	- Building Block?
	- JS Hack?
		- Med brug af REST API?
	- LTI?

- Hvordan gemmes templates?
	- Database tabeller?
		- Nemmere at rette til efter installation
		- Kan resultere en tabel per template, pga. stor variation
	- Klasser?
		- Sv�rere at rette til efter installation
		- Kan speciallicers til detaljerede content typer
		- Kan have en f�lles for�ldre med delte data
		
 
- Skal der v�re forskellige template indstillinger for enkelte fakulteter?
	- Bedre customization
	- �get kompleksitet
	- Forvirring p� tv�rs af fakulteter (eg. en underviser har kurser for b�de HE og ST, men templates opf�rer sig anderledes)

- Hvilke templates?
	- Blank Quiz
	- MCQ 4/n sp�rgsm�l
	